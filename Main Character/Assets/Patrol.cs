﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol : MonoBehaviour {
    function Update()
    {
        if (currentWaypoint & lt; waypoint.length)
     {
            var target : Vector3 = waypoint[currentWaypoint].position;
            var moveDirection : Vector3 = target - transform.position;
            var velocity = rigidbody.velocity;
            if (moveDirection.magnitude & lt; 1)
         {
                currentWaypoint++;
            }
         else
         {
                velocity = moveDirection.normalized * speed;
            }
        }
        rigidbody.velocity = velocity;
    }